Remote Actions
==============

The Remote Actions package, and accompanying REMOTEACTION module, are
used to integrate Orchid Extender's Workflow for Sage 300 with fleetingremote
actions.io to securely collect well formed, validated, data from users or
customers from outside of Sage 300.

The Remote Actions package, and the `Fleeting Forms`_ service that it uses to
generate one time pages, were created and are maintained by 
`Poplar Development`_.

.. _Poplar Development: https://poplars.dev
.. _Fleeting Forms: https://fleetingforms.io
