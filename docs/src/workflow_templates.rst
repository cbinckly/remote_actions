Sample Workflows
===============================================================================

This page contains sample workflows for remote action that you can download
and try for yourself.  

.. note:: 
    
    All the workflows on this page will need to be adjusted for your 
    environment before they will work correctly.

    `Contact us`_ if you'd like a demo.

.. _Contact us: mailto:chris@poplars.dev

Adobe Sign - A/P Invoice Batch Approval Workflow
-------------------------------------------------------------------------------

This workflow uses Adobe Sign to obtain signatures for the approval of A/P 
Invoice Batches.

:download:`A/P Invoice Batch Approval Template <../workflow_templates/AP_Invoice_Batch_Adobe_Sign_Workflow.xlsx>`
