Quickstart
=======================

Want to get up and running with a basic approval form quickly?  This
guide will walkthough the steps required to add a remote approval
to your workflow.

Adding a remote approval to an existing workflow is easy. This is what
needs to be done:

#. Select or create a Message Template to use for the notification to the user
   that a remote approval is available.
#. Select or create a Message Template to use for the form title and content.
#. Identify the Sage users or Extender User Groups that need to receive a 
   notification.
#. Identify the actions the user can take and how the buttons for those actions
   should be labelled.
#. Add the ``REMOTEACTION.SendApprovalFormEmail`` action to your template,
   providing the email notification Message Template, Users, from content
   Message Template, and actions as parameters.

That's all! The next time the workflow is triggered, a remote approval form
will be created and the users notified.

Selecting a Notification Template
---------------------------------

pass

Selecting a From Content Template
---------------------------------

pass

Identifying the Sage Users or Groups
------------------------------------

pass

Identifying the Form Actions
----------------------------

pass

Adding the SendApprovalFormEmail Action
---------------------------------------

pass


