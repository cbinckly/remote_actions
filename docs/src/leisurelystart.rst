Leisurely Start
============================

This guide provides a detailed walkthrough of creating a new workflow
that leverages remote approvals.

To provide some context, it is built around a specific use case:
approving Sale prices for items.

