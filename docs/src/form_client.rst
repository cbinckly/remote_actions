Form Client
==========================

The form client is a support library that includes the 
:py:class:`~remote_actions.services.fleeting_forms.FormClient` class as
well as helper functions that are used across multiple Workflow Actions.

This library is of interest only to those that need to compose custom
workflow actions that create, read, or delete approval forms.

.. automodule:: remote_actions.services.fleeting_forms
    :members:
    :undoc-members:
    :show-inheritance:
