Remote Actions for Orchid Extender
========================================

The Remote Actions package, and accompanying REMOTEACTION module, are used to 
integrate Orchid Extender's Workflow for Sage 300 with fleetingforms.io
to securely collect well formatted, validated, data from users or 
customers from beyond the corporate perimeter.

.. note::

   Remote Action version 6.0 introduces flexible handlers.  If you're
   upgrading or installing new, please have a quick read of the
   `handlers`_ page.

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   src/quickstart
   src/handlers
   src/adobesign
   src/leisurelystart
   src/poller
   src/form_client
   src/workflow
   src/customize_a_form
   src/workflow_templates

The package contains `Workflow actions`_ for Orchid Extender that make it 
simple to create an approval form that can be completed from any computer,
phone, or tablet connected to the internet.

.. _Workflow actions: https://www.orchid.systems/article/introducing-extender-workflow

This documentation includes information on using the included Workflow actions
to `create remote approval forms`_, configuring `handlers`_, 
setting up the `poller`_, a description of
the `form client`_ that is used to connect to `fleetingforms.io`_, and
instructions on how to `customize a form`_ to meet your needs.

If you want to get up and running as quickly as possible, check out the 
`quickstart`_ guide.

Looking for a comprehensive walkthorough of creating a workflow built around a
remote approval?  Try the `leisurelystart`_.

.. _create remote approval forms: src/workflow.html
.. _handlers: src/handlers.html
.. _poller: src/poller.html
.. _form client: src/form_client.html
.. _fleetingforms.io: https://fleetingforms.io
.. _customize a form: src/workflow#.htmlcustomize_a_form
.. _quickstart: src/quickstart.html
.. _leisurelystart: src/leisurelystart.html


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
